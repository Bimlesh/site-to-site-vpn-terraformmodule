module "my_vpc" {
  source              = "../Modules/vpc"
  vpc_cidr            = "10.100.0.0/16"
  enable_dns_hostname = "true"
}

module "my_igws"{
  source           = "../Modules/Igws"
  vpc_id           = "${module.my_vpc.vpc_id}"
  public_subnet_id = "${module.my_subnet.public_subnet_id}"
}

module "my_routetable"{
  source           = "../Modules/routetable"
  vpc_id           = "${module.my_vpc.vpc_id}"
  Igw              = "${module.my_igws.Igw}"
  nat_gw           = "${module.my_igws.nat_gw}"
}

module "my_sgs"{
  source           = "../Modules/Sgs"
  vpc_id           = "${module.my_vpc.vpc_id}"
  destination_cidr = "172.31.0.0/16"
}

module "my_subnet" {
  source        = "../Modules/subnet"
  vpc_id        = "${module.my_vpc.vpc_id}"
  subnet_cidr   = ["10.100.0.0/24", "10.100.1.0/24"]
  azs           = ["${var.aws_region}a", "${var.aws_region}b"]
  tags_subnet   = ["Public_subnet","Private_subnet"]
  route_table_id= ["${module.my_routetable.web-public_rt}","${module.my_routetable.db-private_rt}"]
}


module "my_ec2" {
  source              = "../Modules/ec2"
  ami                 = "ami-0d2692b6acea72ee6"
  key_name            = "test"
  instance_type       = "t2.micro"
  subnet_id           = ["${module.my_subnet.public_subnet_id}","${module.my_subnet.private_subnet_id}"]
  sg_id               = ["${module.my_sgs.public_sg}","${module.my_sgs.private_sg}"]
  associate_public_ip = ["true","false"]
  ec2_tags            = ["web-server","db-server"]
}

module "my_vpn"{
  source           = "../Modules/vpn"
  vpc_id           = "${module.my_vpc.vpc_id}"
  bgp_asn          = "65000"
  client_IP        = "13.59.166.219"
  destination_cidr = "172.31.0.0/16"
  public_rt_id     = "${module.my_routetable.web-public_rt}"
  private_rt_id    = "${module.my_routetable.db-private_rt}"
}
