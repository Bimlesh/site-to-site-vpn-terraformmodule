variable "aws_region"  {
  description = "Region for the VPC"
  default = "ap-south-1"
}
