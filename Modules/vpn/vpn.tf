resource "aws_vpn_gateway" "vpn_gateway" {
  vpc_id = "${var.vpc_id}"
 
    tags = {
     Name = "test_VPN_Gateway"
  }

}

resource "aws_customer_gateway" "customer_gateway" {
  bgp_asn    = "${var.bgp_asn}"
  ip_address = "${var.client_IP}"
  type       = "ipsec.1"
 
tags = {
     Name = "test_Customer_Gateway"
  }

}

resource "aws_vpn_connection" "main" {
  vpn_gateway_id      = "${aws_vpn_gateway.vpn_gateway.id}"
  customer_gateway_id = "${aws_customer_gateway.customer_gateway.id}"
  type                = "ipsec.1"
  static_routes_only  = true

    tags = {
     Name = "test_VPN_Connection"
  }

}

resource "aws_vpn_connection_route" "office" {
  destination_cidr_block = "${var.destination_cidr}"
  vpn_connection_id      = "${aws_vpn_connection.main.id}"
}

resource "aws_vpn_gateway_route_propagation" "public_rp" {
  vpn_gateway_id = "${aws_vpn_gateway.vpn_gateway.id}"
  route_table_id = "${var.public_rt_id}"
}

resource "aws_vpn_gateway_route_propagation" "private_rp" {
  vpn_gateway_id = "${aws_vpn_gateway.vpn_gateway.id}"
  route_table_id = "${var.private_rt_id}"
}

