output "vpn_gateway_id" {
  description = "Virtual Private Gateway ID"
  value       = "${aws_vpn_gateway.vpn_gateway.id}"
}

output "customer_gateway_id" {
  description = "Customer Gateway ID"
  value       = "${aws_customer_gateway.customer_gateway.id}"
}

output "vpn_connection_id" {
  description = "VPN Connection ID"
  value       = "${aws_vpn_connection.main.id}"
}

output "vpn_connection_customer_gateway_configuration" {
  description = "The configuration information for the VPN connection's Customer Gateway (in the native XML format)"
  value       = "${join("", aws_vpn_connection.main.*.customer_gateway_configuration)}"
}
