#Define the Custom VPC
resource "aws_vpc" "default" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = "${var.enable_dns_hostname}"

  tags = {
    Name = "test-vpc"
  }
}
