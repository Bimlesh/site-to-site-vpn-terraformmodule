variable "vpc_cidr" {
  description = "CIDR for the VPC"
  default = "10.100.0.0/16"
}
variable "enable_dns_hostname"{
  description = "Enable Hostname"
  default = "true"
}
