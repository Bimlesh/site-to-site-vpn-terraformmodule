# Define the public subnet
resource "aws_subnet" "test_subnet" {
  count = "${length(var.subnet_cidr)}"
  vpc_id = "${var.vpc_id}"
  cidr_block = "${element(var.subnet_cidr,count.index)}"
  availability_zone = "${element(var.azs,count.index)}"

  tags = {
    Name = "${element(var.tags_subnet,count.index)}"
  }
}

# Assign the route table to the public Subnet
resource "aws_route_table_association" "rtb_association" {
  subnet_id = "${element(aws_subnet.test_subnet.*.id,0)}"
  route_table_id = "${element(var.route_table_id,0)}"
}

# Assign the route table to the private Subnet
resource "aws_route_table_association" "rtb_association22" {
  subnet_id = "${element(aws_subnet.test_subnet.*.id,1)}"
  route_table_id = "${element(var.route_table_id,1)}"
}

output "public_subnet_id" {
  value = "${element(aws_subnet.test_subnet.*.id,0)}"
}

output "private_subnet_id" {
  value = "${ element(aws_subnet.test_subnet.*.id,1) }"
}
