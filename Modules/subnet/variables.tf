variable "azs" {
  type = "list"
}

variable "subnet_cidr" {
  type = "list"
}

variable "vpc_id" {}

variable "tags_subnet" {
  type = "list"
}

variable "route_table_id" {
  type = "list"
}
