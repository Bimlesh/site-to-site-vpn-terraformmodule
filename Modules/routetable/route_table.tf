# Define the route table for public
resource "aws_route_table" "web-public-rt" {
  vpc_id = "${var.vpc_id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${var.Igw}"
  }

  tags = {
    Name = "test_Public_Subnet_RT"
  }
}

# Define the route table for  private
resource "aws_route_table" "db-private-rt" {
  vpc_id = "${var.vpc_id}"

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id  = "${var.nat_gw}"
  }

  tags = {
    Name = "test_Private_Subnet_RT"
  }
}

