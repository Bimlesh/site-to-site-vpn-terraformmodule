resource "aws_instance" "ec2" {
   count = "2"
   ami  = "${var.ami}"
   key_name = "${var.key_name}"
   instance_type = "${var.instance_type}"
   subnet_id = "${element(var.subnet_id,count.index)}"
   vpc_security_group_ids = ["${element(var.sg_id,count.index)}"]
   associate_public_ip_address = "${element(var.associate_public_ip,count.index)}"
   source_dest_check = false

   tags = {
     Name = "${element(var.ec2_tags,count.index)}"
  }
}
