variable "ami" {}

variable "key_name" {}

variable "instance_type" {
 default = "t2.micro"
}

variable "subnet_id" {
  type = "list"
}

variable "sg_id" {
  type = "list"
}

variable "associate_public_ip" {
  type = "list"
}

variable "ec2_tags" {
  type = "list"
}
