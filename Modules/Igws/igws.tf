# Define the internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "test_Vpc_Igw"
  }
}

#Eip for nat gateway
resource "aws_eip" "nat" {
  vpc              = true
  depends_on = ["aws_internet_gateway.gw"]
  
  tags = {
    Name = "test_Eip"
  }

}

#Define NAT gateway
resource "aws_nat_gateway" "gw" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${var.public_subnet_id}"

  tags = {
    Name = "test_ Natgw"
  }
}

