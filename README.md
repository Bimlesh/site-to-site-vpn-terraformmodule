## Site-to-site VPN Connection
This Terraform code will build 
- VPC
- 2 Subnets (Public and Private)
- Igw
- NAT Gateway
- Elastic Ip
- Route tabe (Public and Private)
- Security Groups (Public and Private)
- Ec2 instance (one in Public and one private subnet)
- Customer Gateway
- Virtual Private Gateway
- VPN

##Pre-Requisite
- Aws cli Configured and Installed
- Terraform Installed 

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| aws_region | Region for the VPC | string  | `ap-south-1` | yes |
| vpc_cidr | CIDR for the VPC | string | `-` | yes |
| public_subnet_cidr | CIDR for the public subnet | string | `-`  | yes |
| private_subnet_cidr | CIDR for the private subnet | string | `-` | yes |
| ami | Amazon Ubuntu AMI | string | `-` | no |
| key_name | Name of the existing key_pair | string | - | no |
| destination_cidr | Name of the destination(client) subnet | string | - | yes |
| client_IP | Name of the destination public ip  | string | `` | yes |
| instance_type | Instance type u want | string | `t2.micro`| yes |
| bgp_asn | The gateway's Border Gateway Protocol (BGP) Autonomous System Number (ASN) | string | `65000` | no |

## Usage

```
terraform init
terraform plan
terraform apply
```